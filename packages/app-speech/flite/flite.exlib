# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="${PNV}-release"

export_exlib_phases src_prepare

SUMMARY="A small, fast run-time speech synthesis engine"
DESCRIPTION="
It's primarily designed for small embedded machines and/or large servers.
Flite is designed as an alternative synthesis engine to Festival for voices
built using the FestVox suite of voice building tools.
"
HOMEPAGE="http://www.festvox.org/flite"
DOWNLOADS="${HOMEPAGE}/packed/${PN}-$(ever range -2)/${MY_PNV}.tar.bz2"

LICENCES="
    BSD-3 [[ note = [ src/cg/cst_*, see COPYING ] ]]
    BSD-4
"
SLOT="0"
MYOPTIONS="
    alsa
    pulseaudio

    ( alsa pulseaudio ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        alsa? ( sys-sound/alsa-lib )
        pulseaudio? ( media-sound/pulseaudio )
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    --enable-shared
)
DEFAULT_SRC_CONFIGURE_OPTIONS+=(
    'alsa --with-audio=alsa'
    'pulseaudio --with-audio=pulseaudio'
)

flite_src_prepare() {
    default

    edo sed -e 's: ar : $(AR) :' -i config/common_make_rules
}

