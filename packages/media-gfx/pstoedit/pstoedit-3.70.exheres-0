# Copyright 2009, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Pstoedit translates PostScript and PDF graphics into other vector formats"
HOMEPAGE="http://www.${PN}.net"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    pptx [[ description = [ Support for generating PowerPoint pptx files ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: graphicsmagick imagemagick ) [[
        *description = [ Software suites to create, edit, and compose bitmap images ]
        number-selected = at-least-one
    ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-text/ghostscript
        media-libs/gd
        media-libs/libpng:=
        media-libs/plotutils[>=2.3]
        pptx? ( app-arch/libzip )
        providers:graphicsmagick? ( media-gfx/GraphicsMagick[imagemagick][>=1.0.6] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:imagemagick? ( media-gfx/ImageMagick[>=1.0.6] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-libplot
    --with-magick
    --without-emf
    --without-swf
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( pptx )

