# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A completely open audio compression format"
DESCRIPTION="
WavPack is a completely open audio compression format providing lossless, high-quality lossy, and a
unique hybrid compression mode. Although the technology is loosely based on previous versions of
WavPack, the new version 4 format has been designed from the ground up to offer unparalleled
performance and functionality.

In the default lossless mode WavPack acts just like a WinZip compressor for audio files. However,
unlike MP3 or WMA encoding which can affect the sound quality, not a single bit of the original
information is lost, so there's no chance of degradation. This makes lossless mode ideal for
archiving audio material or any other situation where quality is paramount. The compression ratio
depends on the source material, but generally is between 30% and 70%.

The hybrid mode provides all the advantages of lossless compression with an additional bonus.
Instead of creating a single file, this mode creates both a relatively small, high-quality lossy
file that can be used all by itself, and a 'correction' file that (when combined with the lossy
file) provides full lossless restoration. For some users this means never having to choose between
lossless and lossy compression!
"
HOMEPAGE="https://www.wavpack.com"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.bz2"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        providers:openssl? ( dev-libs/openssl:= )
        providers:libressl? ( dev-libs/libressl:= )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-apps
    --enable-dsd
    # Use libcrypto instead of an included MD5 implementation
    --enable-libcrypto
    --disable-legacy
    --disable-static
)
DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests --disable-tests' )

DEFAULT_SRC_COMPILE_PARAMS=(
    # TODO(sardemff7) Patch upstream to just use pkgincludedir
    # We consider them as arch-dependent
    wpincludedir='${pkgincludedir}'
)

DEFAULT_SRC_INSTALL_PARAMS=(
    # We consider them as arch-dependent
    wpincludedir='${pkgincludedir}'
)

src_test_expensive() {
    edo ./cli/wvtest --default
}

