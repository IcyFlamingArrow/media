# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2014 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix='https://code.videolan.org' user='videolan' tag=b86ae3c66f51ac9eab5ab7ad09a9d62e67961b8a suffix=tar.bz2 new_download_scheme=true ]
require bash-completion

export_exlib_phases src_configure src_test src_install

SUMMARY="Library for encoding H264/AVC video streams"
HOMEPAGE+=" https://www.videolan.org/developers/${PN}.html"

LICENCES="GPL-2 GPL-3"
SLOT="0"
MYOPTIONS="
    visualisation [[ description = [ Visualize the encoding process ] ]]
    platform: amd64 x86
"

DEPENDENCIES="
    build:
        platform:amd64? ( dev-lang/nasm[>=2.13] )
        platform:x86? ( dev-lang/nasm[>=2.13] )
    build+run:
        visualisation? ( x11-libs/libX11 )
"

_x264_assembler() {
    case "$(exhost --target)" in
    i686-*|x86_64-*)
        echo nasm
    ;;
    *)
        echo $(exhost --tool-prefix)cc
    ;;
    esac
}

x264_src_configure() {
    export AS=$(_x264_assembler)

    local myconf=(
        # prefixed string executable isn\'t found otherwise
        --cross-prefix=$(exhost --tool-prefix)
        --enable-shared
        --disable-avs
        --disable-bashcompletion
        --disable-ffms
        --disable-gpac
        --disable-lavf
        --disable-swscale
    )

    # --disable-visualize doesn't exist so don't use option_enable
    option visualisation && myconf+=( "--enable-visualize" )

    econf "${myconf[@]}"
}

x264_src_test() {
    :
}

x264_src_install() {
    default

    dobashcompletion tools/bash-autocomplete.sh x264
}

