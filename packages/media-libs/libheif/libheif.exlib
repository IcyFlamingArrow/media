# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=strukturag release=v${PV} suffix=tar.gz ]
require freedesktop-mime

SUMMARY="An image file format employing HEVC (h.265) image coding"
DESCRIPTION="
libheif makes use of libde265 for the actual image decoding and x265 for
encoding. Alternative codecs for, e.g., AVC and JPEG can be provided as
plugins.
"

LICENCES="
    GPL-3  [[ note = [ sample applications ] ]]
    LGPL-3 [[ note = library ]]
"
SLOT="0"
MYOPTIONS="
    aom [[ description = [ Use aom for en-/decoding AVIF images ] ]]
    rav1e [[ description = [ Use rav1e for encoding AVIF images ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/dav1d
        media-libs/libde265
        media-libs/libpng:=
        media-libs/x265:=
        aom? ( media-libs/aom )
        rav1e? ( media-video/rav1e )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-examples
    --enable-multithreading
    # decoder for HEIF
    --enable-libde265
    # encoder for HEIF
    --enable-x265
    --disable-gdk-pixbuf
    --disable-go
    --disable-libfuzzer
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    # en-/decoder for AVIF
    aom
    # encoder for AVIF
    rav1e
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-tests --disable-tests'
)

