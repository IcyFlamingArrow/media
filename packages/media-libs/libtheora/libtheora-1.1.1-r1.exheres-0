# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2009 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.13 1.12 1.11 1.10 ] ]

SUMMARY="Theora video compression codec"
HOMEPAGE="http://theora.org"
DOWNLOADS="http://downloads.xiph.org/releases/theora/${PNV}.tar.bz2"

UPSTREAM_CHANGELOG="http://svn.xiph.org/trunk/theora/CHANGES [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug [[ description = [ Telemetry output showing quantization choice and bitrate usage in the frame ] ]]
    doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        media-libs/libogg
        media-libs/libvorbis
        debug? ( x11-libs/cairo )
"

AT_M4DIR=( 'm4' )

DEFAULT_SRC_PREPARE_PATCHES=(
    -p0 "${FILES}"/${PNV}-configure.patch
    -p0 "${FILES}"/${PNV}-docdir.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # FIXME: see what packages will provide all those tools
    --disable-spec

    --disable-examples
    --disable-valgrind-testing
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc' )

