# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=AOMediaCodec tag=v${PV} ] cmake
require alternatives

export_exlib_phases src_test src_install

SUMMARY="A friendly, portable C implementation of the AV1 Image File Format"

LICENCES="BSD-2"
MYOPTIONS="
    apps [[ description = [ Build the avifdec and avifenc applications ] ]]
    yuv  [[ description = [ Will use libyuv fro conversion from YUV to RGP ] ]]

    apps? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build+run:
        media-libs/dav1d
        media-video/rav1e
        apps? (
            media-libs/libpng
            sys-libs/zlib
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        )
        yuv? ( media-libs/libyuv )
    run:
        !media-libs/libavif:0[<0.8.4-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE

    # Codecs (maybe we could add providers, for now I just picked the most
    # sensible options)
    -DAVIF_CODEC_AOM:BOOL=FALSE     # reference impl, but probably slow
    -DAVIF_CODEC_DAV1D:BOOL=TRUE    # for decoding
    -DAVIF_CODEC_LIBGAV1:BOOL=FALSE # another decoder, unwritten
    -DAVIF_CODEC_RAV1E:BOOL=TRUE    # for encondig
    -DAVIF_CODEC_SVT:BOOL=FALSE     # another encoder, unwritten

    -DAVIF_BUILD_EXAMPLES:BOOL=FALSE
    -DAVIF_ENABLE_COVERAGE:BOOL=FALSE
    -DAVIF_ENABLE_WERROR:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'yuv libyuv' )
CMAKE_SRC_CONFIGURE_OPTIONS+=( 'apps AVIF_BUILD_APPS' )

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DAVIF_BUILD_TESTS:BOOL=TRUE -DAVIF_BUILD_TESTS:BOOL=FALSE'
)

libavif_src_test() {
    # Apparently some tests use absolute metrics which are unreliable with
    # different codecs versions, eg. they started to fail with rav1e >= 0.5.0
    # https://github.com/AOMediaCodec/libavif/issues/798
    #emake avif_test_all
    edo ./aviftest ../tests/data --io-only
}

libavif_src_install() {
    local alternatives=() host=$(exhost --target)

    default

    alternatives+=(
        /usr/${host}/include/avif           avif-${SLOT}
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/cmake/${PN}/${PN}-config.cmake         ${PN}-${SLOT}-config.cmake
        /usr/${host}/lib/cmake/${PN}/${PN}-config-none.cmake    ${PN}-${SLOT}-config-none.cmake
        /usr/${host}/lib/cmake/${PN}/${PN}-config-version.cmake ${PN}-${SLOT}-config-version.cmake
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    if option apps ; then
        alternatives+=(
            /usr/${host}/bin/avifdec avifdec-${SLOT}
            /usr/${host}/bin/avifenc avifenc-${SLOT}
        )
    fi

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}
