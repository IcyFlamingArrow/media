# Copyright 2010 Yury G. Kudryashov <urkud@ya.ru>
# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'opencv-2.0.0-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

MY_PN=OpenCV

require cmake flag-o-matic github

require python [ blacklist=none multibuild=true multiunpack=false with_opt=true ]

SUMMARY="A collection of algorithms and sample code for various computer vision problems"
HOMEPAGE="https://${PN}.org"
# 460 MiB
#DOWNLOADS+=" https://github.com/Itseez/${PN}_extra/archive/${PV}.tar.gz -> ${PN}_extra-${PV}"
DOWNLOADS+="
    contrib? ( https://github.com/${PN}/${PN}_contrib/archive/${PV}.tar.gz -> ${PN}_contrib-${PV}.tar.gz )
"

REMOTE_IDS="sourceforge:${PN}library"

LICENCES="
    Apache-2.0
    xine? ( GPL-2 )
"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"

FILTERED_PYTHON3_ABIS="${PYTHON_FILTERED_ABIS/2.7 }"

# All subdirectories in modules/ minus xfeatures2d, which contains no-free parts.
contrib_modules="
    alphamat
    bgsegm
    bioinspired
    ccalib
    cvv
    datasets
    dpm
    face
    fuzzy
    img_hash
    intensity_transform
    line_descriptor
    optflow
    phase_unwrapping
    plot
    quality
    rapid
    reg
    rgbd
    saliency
    shape
    surface_matching
    text
    tracking
    videostab
    viz
    ximgproc
    xobjdetect
    xphoto
"

MYOPTIONS="
    camera [[ description = [ Capture using your camera device via libgphoto2 ] ]]
    contrib [[ description = [ Extra modules, possibly without stable API and not well-tested ] ]]
    doc
    examples
    ffmpeg
    gstreamer
    gtk
    ieee1394
    java
    jpeg2000
    opencl [[ description = [ Hardware acceleration via OpenCL ] ]]
    openexr
    openmp [[ description = [ Support for Open Multi-Processing ] ]]
    qt5 [[ description = [ Adds support for the Qt GUI/Application Toolkit version 5.x ] ]]
    threads
    tiff
    vtk [[ description = [ VTK support for the Viz contrib module ] ]]
    vulkan [[ description = [ Support for the Vulkan API ] ]]
    webp [[ description = [ Support for the WebP image format ] ]]
    xine

    ( contrib_modules: ( ${contrib_modules} ) [[ *requires = contrib ]] )
    ( contrib_modules: viz ) [[ requires = vtk ]]
    contrib? ( ( contrib_modules: ${contrib_modules} ) [[ number-selected = at-least-one ]] )

    ( gtk qt5 ) [[ number-selected = at-most-one ]]
    qt5? (
        opengl [[ description = [ Use OpenGL for rendering in Qt5 based GUI ] ]]
    )

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]

    python? (
        ( python_abis: ${FILTERED_PYTHON3_ABIS} ) [[ number-selected = exactly-one ]]
    )
"

# FIXME: Download this data if tests are enabled and run tests
RESTRICT=test

DEPENDENCIES="
    build:
        sci-libs/eigen:3 [[ note = [ Optional, enable since this is a build-time header-only dependency. ] ]]
        sys-libs/zlib[>=1.2.3]
        doc? (
            dev-python/Sphinx
            dev-texlive/texlive-latex
        )
        gtk? ( virtual/pkg-config )
        vulkan? ( sys-libs/vulkan-headers )
    build+run:
        media-libs/libpng:=
        camera? ( media-libs/libgphoto2 )
        ffmpeg? ( media/ffmpeg )
        gstreamer? (
            dev-libs/glib:2
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        gtk? (
            dev-libs/glib:2 [[ note = gthread ]]
            x11-libs/gtk+:3
        )
        ieee1394? (
            media-libs/libdc1394:2
            media-libs/libraw1394[>=1.2.0]
        )
        java? (
            dev-java/apache-ant
            virtual/jdk:=[>=1.6] [[ note = [ recommended version spec ] ]]
        )
        jpeg2000? ( media-libs/OpenJPEG:2[>=2.3.1] )
        opencl? ( dev-libs/opencl-headers )
        openexr? (
            media-libs/imath
            media-libs/openexr[>=3]
        )
        openmp? ( sys-libs/libgomp:* )
        python? ( dev-python/numpy[python_abis:*(-)?] )
        qt5? (
            x11-libs/qtbase:5
            opengl? (
                dev-libs/libglvnd
                x11-dri/glu
            )
        )
        threads? ( dev-libs/tbb )
        tiff? ( media-libs/tiff )
        vtk? ( sci-libs/vtk[>=5.8.0][qt5=] )
        webp? ( media-libs/libwebp:= )
        xine? ( media-libs/xine-lib )
        contrib_modules:text? ( app-text/tesseract )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/971dacaf41941eddf67c6cba8f966edf16d1386e.patch
)

pkg_setup() {
    # Force -DNDEBUG because upstream has errors in their debug code
    append-flags -DNDEBUG
}

configure_one_multibuild() {
    # src_configure already did all there is to do
    :
}

src_configure() {
    # FIXME: ipp support? (library is non-free / costs money)
    # FIXME: unicap support
    local p cmakeparams=(
        # Fail if actual configuration doesn't match the requested one
        -DENABLE_CONFIG_VERIFICATION:BOOL=TRUE

        -DBUILD_IPP_IW:BOOL=FALSE
        -DBUILD_ITT:BOOL=FALSE
        -DBUILD_JASPER:BOOL=FALSE
        -DBUILD_JPEG:BOOL=FALSE
        -DBUILD_OBJC:BOOL=FALSE
        -DBUILD_OPENEXR:BOOL=FALSE
        -DBUILD_OPENJPEG:BOOL=FALSE
        -DBUILD_PNG:BOOL=FALSE
        -DBUILD_PROTOBUF:BOOL=FALSE
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DBUILD_TBB:BOOL=FALSE
        -DBUILD_TIFF:BOOL=FALSE
        -DBUILD_WEBP:BOOL=FALSE
        -DBUILD_ZLIB:BOOL=FALSE
        -DBUILD_opencv_dnn:BOOL=FALSE
        -DBUILD_opencv_gapi:BOOL=FALSE
        -DCMAKE_SKIP_RPATH:BOOL=FALSE
        -DOPENCV_DOC_INSTALL_PATH:PATH=/usr/share/doc/${PNVR}
        -DOPENCV_GENERATE_PKGCONFIG:BOOL=TRUE
        -DOPENCV_OTHER_INSTALL_PATH:PATH=/usr/share/OpenCV
        -DOPENCV_SAMPLES_SRC_INSTALL_PATH:PATH=/usr/share/OpenCV/samples
        -DOPENCV_TEST_DATA_INSTALL_PATH:PATH=/usr/share/OpenCV/testdata
        -DPKG_CONFIG_EXECUTABLE:PATH=${PKG_CONFIG}
        -DENABLE_CCACHE:BOOL=FALSE
        -DENABLE_DYNAMIC_CUDA:BOOL=FALSE
        -DENABLE_OMIT_FRAME_POINTER:BOOL=FALSE
        -DENABLE_PRECOMPILED_HEADERS:BOOL=FALSE
        -DENABLE_PROFILING:BOOL=FALSE
        -DWITH_ANDROID_MEDIANDK:BOOL=FALSE
        -DWITH_ANDROID_NATIVE_CAMERA:BOOL=FALSE
        -DWITH_ARAVIS:BOOL=FALSE
        -DWITH_CAP_IOS:BOOL=FALSE
        -DWITH_CAROTENE:BOOL=FALSE
        -DWITH_CPUFEATURES:BOOL=FALSE
        -DWITH_CUDA:BOOL=FALSE
        -DWITH_CUDNN:BOOL=FALSE
        -DWITH_CUBLAS:BOOL=FALSE
        -DWITH_CUFFT:BOOL=FALSE
        -DWITH_EIGEN:BOOL=TRUE
        -DWITH_GDAL:BOOL=FALSE
        -DWITH_GDCM:BOOL=FALSE
        -DWITH_GTK_2_X:BOOL=FALSE
        -DWITH_HALIDE:BOOL=FALSE
        -DWITH_HPX:BOOL=FALSE
        -DWITH_INF_ENGINE:BOOL=FALSE
        -DWITH_IPP:BOOL=FALSE
        -DWITH_ITT:BOOL=FALSE
        -DWITH_JASPER:BOOL=FALSE
        -DWITH_JPEG:BOOL=TRUE
        -DWITH_LAPACK:BOOL=FALSE
        -DWITH_LIBREALSENSE:BOOL=FALSE
        -DWITH_NVCUVID:BOOL=FALSE
        -DWITH_MATLAB:BOOL=FALSE
        -DWITH_MFX:BOOL=FALSE
        -DWITH_NGRAPH:BOOL=FALSE
        -DWITH_ONNX:BOOL=FALSE
        -DWITH_OPENCLAMDFFT:BOOL=FALSE
        -DWITH_OPENCLAMDBLAS:BOOL=FALSE
        -DWITH_OPENNI:BOOL=FALSE
        -DWITH_OPENNI2:BOOL=FALSE
        -DWITH_OPENVX:BOOL=FALSE
        -DWITH_PNG:BOOL=TRUE
        -DWITH_PROTOBUF:BOOL=FALSE
        -DWITH_PVAPI:BOOL=FALSE
        -DWITH_TENGINE:BOOL=FALSE
        -DWITH_UEYE:BOOL=FALSE
        -DWITH_UNICAP:BOOL=FALSE
        -DWITH_VA:BOOL=FALSE
        -DWITH_VA_INTEL:BOOL=FALSE
        -DWITH_V4L:BOOL=TRUE
        -DWITH_XIMEA:BOOL=FALSE
        $(cmake_build doc DOCS)
        $(cmake_build EXAMPLES)
        $(cmake_build java JAVA)
        $(cmake_build python NEW_PYTHON_SUPPORT)
        $(cmake_option examples INSTALL_C_EXAMPLES)
        $(cmake_with camera GPHOTO2)
        $(cmake_with FFMPEG)
        $(cmake_with GSTREAMER)
        $(cmake_with GTK)
        $(cmake_with ieee1394 1394)
        $(cmake_with JAVA)
        $(cmake_with jpeg2000 OPENJPEG)
        $(cmake_with OPENCL)
        $(cmake_with OPENEXR)
        $(cmake_with OPENMP)
        $(cmake_with qt5 QT)
        $(cmake_with threads TBB)
        $(cmake_with TIFF)
        $(cmake_with VTK)
        $(cmake_with VULKAN)
        $(cmake_with WEBP)
        $(cmake_with XINE)
        $(expecting_tests -DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE)
    )

    if option qt5; then
        cmakeparams+=(
            $(cmake_with OPENGL)
        )
    fi;

    if option contrib ; then
        cmakeparams+=(
            -DOPENCV_EXTRA_MODULES_PATH="${WORKBASE}"/opencv_contrib-${PV}/modules
            -DBUILD_opencv_aruco:BOOL=FALSE
            -DBUILD_opencv_cnn_3dobj:BOOL=FALSE
            -DBUILD_opencv_cudaarithm:BOOL=FALSE
            -DBUILD_opencv_cudabgsegm:BOOL=FALSE
            -DBUILD_opencv_cudacodec:BOOL=FALSE
            -DBUILD_opencv_cudafeatures2d:BOOL=FALSE
            -DBUILD_opencv_cudafilters:BOOL=FALSE
            -DBUILD_opencv_cudalegacy:BOOL=FALSE
            -DBUILD_opencv_cudaobjdetect:BOOL=FALSE
            -DBUILD_opencv_cudaoptflow:BOOL=FALSE
            -DBUILD_opencv_cudastereo:BOOL=FALSE
            -DBUILD_opencv_cudawarping:BOOL=FALSE
            -DBUILD_opencv_cudev:BOOL=FALSE
            -DBUILD_opencv_dnn_objdetect:BOOL=FALSE
            -DBUILD_opencv_dnn_superres:BOOL=FALSE
            -DBUILD_opencv_dnns_easily_fooled:BOOL=FALSE
            -DBUILD_opencv_freetype:BOOL=FALSE
            -DBUILD_opencv_fuzzy:BOOL=FALSE
            -DBUILD_opencv_hdf:BOOL=FALSE
            -DBUILD_opencv_hfs:BOOL=FALSE
            -DBUILD_opencv_julia:BOOL=FALSE
            -DBUILD_opencv_matlab:BOOL=FALSE
            -DBUILD_opencv_mcc:BOOL=FALSE
            -DBUILD_opencv_ovis:BOOL=FALSE
            -DBUILD_opencv_phase_unwrapping:BOOL=FALSE
            -DBUILD_opencv_plot:BOOL=FALSE
            -DBUILD_opencv_sfm:BOOL=FALSE
            -DBUILD_opencv_stereo:BOOL=FALSE
            -DBUILD_opencv_structured_light:BOOL=FALSE
            -DBUILD_opencv_superres:BOOL=FALSE
            -DBUILD_opencv_wechat_qrcode:BOOL=FALSE
            -DBUILD_opencv_xfeatures2d:BOOL=FALSE
        )
        local module
        for module in ${contrib_modules} ; do
            cmakeparams+=(
                $(cmake_build contrib_modules:${module} opencv_${module})
            )
        done
    fi

    cmakeparams+=( $(cmake_build python_abis:2.7 opencv_python2) )

    for abi in ${FILTERED_PYTHON3_ABIS}; do
        optionq python_abis:${abi} && PYTHON3_ABI=${abi}
    done
    if [[ -n ${PYTHON3_ABI} ]]; then
        cmakeparams+=(
            -DBUILD_opencv_python3:BOOL=TRUE
            -DOPENCV_PYTHON3_VERSION:STRING=${PYTHON3_ABI}
        )
    else
        cmakeparams+=( -DBUILD_opencv_python3:BOOL=FALSE )
    fi

    ecmake "${cmakeparams[@]}"

    # make sure multibuild directories are created, otherwise running `easy-multibuild_run_phase` in
    # src_install to bytecompile all Python ABIs will fail
    easy-multibuild_run_phase
}

src_compile() {
    default
}

src_test() {
    # see notice about tests above
    for unittest in $(find bin/ -type f -name "opencv_test_*") ; do
        edo ./${unittest}
    done
}

install_one_multibuild() {
    python_bytecompile
}

src_install() {
    cmake_src_install

    option python && easy-multibuild_run_phase
}

