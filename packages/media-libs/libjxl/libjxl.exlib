# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] cmake

export_exlib_phases src_unpack src_compile src_install

SUMMARY="JPEG XL image format reference implementation "
DESCRIPTION="
JPEG XL (.jxl) is the next-generation, general-purpose image compression codec
by the JPEG committee. It was based on Google's Pik proposal and Cloudinary's
FUIF proposal (which itself was based on FLIF)."

HOMEPAGE="https://jpegxl.info/ ${HOMEPAGE}"

# No releases, rev taken from deps.sh
LODEPNG_REV=48e5364ef48ec2408f44c727657ac1b6703185f8
DOWNLOADS+="
    https://github.com/lvandeve/lodepng/archive/${LODEPNG_REV}.tar.gz \
    -> lodepng-${LODEPNG_REV}.tar.gz
"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    doc gif openexr

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
        dev-lang/python:*[>=3]
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        app-arch/brotli
        dev-libs/highway
        media-libs/lcms2[>=2.10]
        media-libs/libpng:=
        gif? ( media-libs/giflib:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sys-libs/zlib
    test:
        dev-cpp/gtest[googlemock]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Would bring automagic dep on libavif and libwebp
    -DJPEGXL_ENABLE_BENCHMARK:BOOL=FALSE
    -DJPEGXL_ENABLE_DEVTOOLS:BOOL=FALSE
    # Don't have an install target
    -DJPEGXL_ENABLE_EXAMPLES:BOOL=FALSE
    -DJPEGXL_ENABLE_FUZZERS:BOOL=FALSE
    -DJPEGXL_ENABLE_JNI:BOOL=FALSE
    -DJPEGXL_ENABLE_MANPAGES:BOOL=TRUE
    -DJPEGXL_ENABLE_PROFILER:BOOL=FALSE
    # Unpackaged, no releases
    -DJPEGXL_ENABLE_SJPEG:BOOL=FALSE
    # Same here
    -DJPEGXL_ENABLE_SKCMS:BOOL=FALSE
    -DJPEGXL_ENABLE_TCMALLOC=FALSE
    -DJPEGXL_ENABLE_TOOLS:BOOL=TRUE
    # Don't have an install target, would need Qt5
    -DJPEGXL_ENABLE_VIEWERS:BOOL=FALSE
    -DJPEGXL_FORCE_SYSTEM_BROTLI:BOOL=TRUE
    -DJPEGXL_FORCE_SYSTEM_GTEST:BOOL=TRUE
    -DJPEGXL_FORCE_SYSTEM_HWY:BOOL=TRUE
    -DJPEGXL_FORCE_SYSTEM_LCMS2:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'doc Doxygen'
    GIF
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'openexr JPEGXL_ENABLE_OPENEXR'
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

# Disable two failing tests (out of 894)
DEFAULT_SRC_TEST_PARAMS+=(
    ARGS+="-E '(RobustStatisticsTest.CleanLine|DecodeTest.PixelTestWithICCProfileLossy)'"
)

libjxl_src_unpack() {
    cmake_src_unpack

    edo mv lodepng-${LODEPNG_REV}/* "${CMAKE_SOURCE}"/third_party/lodepng
}

libjxl_src_compile() {
    default

    option doc && emake doc
}

libjxl_src_install() {
    cmake_src_install

    option doc && dodoc -r html
}

