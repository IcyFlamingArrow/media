# Copyright 2008 Richard Brown
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'imlib2-1.4.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

SUMMARY="Image Processing Library"
HOMEPAGE="https://docs.enlightenment.org/api/${PN}/html/pages.html"
DOWNLOADS="mirror://sourceforge/enlightenment/${PNV}.tar.xz"

LICENCES="${PN}"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    tiff
    webp
    X
    ( platform: amd64 x86 )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( x86_cpu_features: mmx )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        X? (
            x11-proto/xorgproto
        )
    build+run:
        app-arch/bzip2
        media-libs/freetype:=
        media-libs/giflib:=
        media-libs/libpng:=
        sys-libs/zlib
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff[>=4] )
        webp? ( media-libs/libwebp:= )
        X? (
            x11-libs/libX11
            x11-libs/libxcb[>=1.9]
            x11-libs/libXext
        )
"

src_configure() {
    local myconf=()

    myconf+=(
        --enable-visibility-hiding
        --disable-gcc-asan
        --disable-static
        --with-bzip2
        --with-gif
        --with-jpeg
        --with-png
        --with-zlib
        --without-id3
        $(option_with tiff)
        $(option_with webp)
        $(option_with X x)
        $(option_with X x-shm-fd)
        $(option_enable platform:amd64)
    )

    if [[ $(exhost --target) == x86_64-pc-linux-gnu ]] ; then
        myconf+=( --enable-mmx )
    elif [[ $(exhost --target) == i686-pc-linux-gnu ]] ; then
        myconf+=( option_with x86_cpu_features:mmx )
    fi

    econf "${myconf[@]}"
}

