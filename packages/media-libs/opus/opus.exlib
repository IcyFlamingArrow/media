# Copyright 2012 Lasse Brun <bruners@gmail.com>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="https://git.xiph.org/opus.git"
    require scm-git
    require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
else
    DOWNLOADS="https://archive.mozilla.org/pub/${PN}/${PNV}.tar.gz"
fi

SUMMARY="The Opus Codec incorporates technology from Skype's SILK codec and Xiph.Org's CELT codec"
DESCRIPTION="
The Opus codec is designed for interactive speech and audio transmission over the Internet. It is
designed by the IETF Codec Working Group and incorporates technology from Skype's SILK codec and
Xiph.Org's CELT codec.

The Opus codec is designed to handle a wide range of interactive audio applications, including Voice
over IP, videoconferencing, in-game chat, and even remote live music performances. It can scale from
low bit-rate narrowband speech to very high quality stereo music.
"
HOMEPAGE="https://www.opus-codec.org"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen[dot] )
    build+run:
        (
            !media-libs/opus:0.9
            !media-libs/opus:1.0
        ) [[
            *description = [ Old non-working slot scheme ]
            *resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-custom-modes
    --enable-hardening
    --enable-intrinsics
    --enable-rfc8251
    --disable-fuzzing
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( doc )

