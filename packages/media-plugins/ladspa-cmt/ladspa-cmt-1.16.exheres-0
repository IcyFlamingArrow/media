# Copyright 2009, 2010 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="CMT (computer music toolkit) LADSPA library plugins"
DESCRIPTION="
The Computer Music Toolkit (CMT) is a collection of LADSPA plugins for use with
software synthesis and recording packages on Linux.
"
HOMEPAGE="http://www.ladspa.org/"
DOWNLOADS="${HOMEPAGE}/download/cmt_src_${PV}.tgz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        media-libs/ladspa-sdk
"

RESTRICT="test" # Tests play audio
WORK="${WORKBASE}"/cmt/src

src_prepare() {
    default

    # Respect DESTDIR
    edo sed -i \
            -e "s:/usr/lib/ladspa:\$(DESTDIR)/usr/${LIBDIR}/ladspa:" \
            makefile

    # Respect CC, CXX, CFLAGS
    edo sed -i \
            -e "s:cc:${CC}:" \
            -e "s:c++:${CXX}:" \
            -e "s:-O3:${CFLAGS}:" \
            makefile

    # Don't strip
    edo sed -i \
            -e "/strip/d" \
            makefile
}

src_install() {
    dodir /usr/${LIBDIR}/ladspa
    default
    dodoc ../README
}

