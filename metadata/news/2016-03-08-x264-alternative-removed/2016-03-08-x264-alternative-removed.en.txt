Title: The x264 10-bit and 8-bit alternatives were replaced
Author: Johannes Nixdorf <mixi@exherbo.org>
Content-Type: text/plain
Posted: 2016-03-08
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: media-libs/x264[<20151011-r1]

The 10-bit and 8-bit alternatives for x264 were removed, so software requiring
a specific choice can depend on it being the system default.

For software that supports both (e.g. ffmpeg or libav) the other version can be
selected at runtime by using LD_LIBRARY_PATH as follows:

 * 10-bit if 8-bit is the system default:
        LD_LIBRARY_PATH=/usr/host/lib/x264-10 ffmpeg […]

 * 8-bit if 10-bit is the system default:
        LD_LIBRARY_PATH=/usr/host/lib/x264-8 ffmpeg […]
